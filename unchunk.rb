#!/usr/bin/env ruby

def usage()
	puts "Usage:"
	puts "	" + $0 + " IN.IDX IN.BIN outpfx"
end

puts "UnCHUNK -- a Tekken 6 datafile extractor"
puts "Copyight (C) 2010 trap15"
puts "All rights reserved."

unless ARGV.length >= 3
	print "Invalid arguments. "
	usage()
	exit
end

def readU64(d, i)
	sec = d[i...i+8].unpack("NN")
	return (sec[0] << 32) | sec[1]
end

def readU32(d, i)
	sec = d[i...i+4].unpack("N")
	return sec[0]
end

def readU16(d, i)
	sec = d[i...i+2].unpack("n")
	return sec[0]
end

def readU8(d, i)
	sec = d[i...i+1].unpack("C")
	return sec[0]
end

idxf = File.new(ARGV[0], "r")
idx = idxf.read()
idxf.close

offset = []
size = []
count = (idx.length / 8).floor

puts "Index count: " + count.to_s()

for i in (0...count)
	offset.push(readU32(idx, i * 8) << 7)
	size.push(readU32(idx, i * 8 + 4))
end

for i in (0...count)
	puts "Item %d:" % i
	puts "	Offset  : 0x%08X" % offset[i]
	puts "	Size    : 0x%08X" % size[i]
end

puts "Extracting files..."

dataf = File.new(ARGV[1], "r")
data = dataf.read()
dataf.close

for i in (0...count)
	puts "File %d" % i
	outf = File.new(ARGV[2] + "_%d.BIN" % i, "w+")
	outf.write(data[offset[i]...offset[i] + size[i]])
	outf.close
end

