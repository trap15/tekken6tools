#!/usr/bin/env ruby

def usage()
	puts "Usage:"
	puts "	" + $0 + " in.bin outpfx"
end

puts "UnPAC -- a Tekken 6 PAC\\x01 file extractor"
puts "Copyight (C) 2010 trap15"
puts "All rights reserved."

unless ARGV.length >= 1
	print "Invalid arguments. "
	usage()
	exit
end

def align(v, a)
	if (v % a) != 0
		v += a - (v % a)
	end
	return v
end

def readU64(d, i)
	sec = d[i...i+8].unpack("NN")
	return (sec[0] << 32) | sec[1]
end

def readU32(d, i)
	sec = d[i...i+4].unpack("N")
	return sec[0]
end

def readU16(d, i)
	sec = d[i...i+2].unpack("n")
	return sec[0]
end

def readU8(d, i)
	sec = d[i...i+1].unpack("C")
	return sec[0]
end

def readEU64(d, i)
	sec = d[i...i+8].unpack("VV")
	return (sec[1] << 32) | sec[0]
end

def readEU32(d, i)
	sec = d[i...i+4].unpack("V")
	return sec[0]
end

def readEU16(d, i)
	sec = d[i...i+2].unpack("v")
	return sec[0]
end

def readEU8(d, i)
	sec = d[i...i+1].unpack("C")
	return sec[0]
end

# TODO: Figure out wtf the PAC\x01 format actually is and how it's encoded...
def handle_pac(data, pfx)
	magic = readU32(data, 0)
	unless magic == 0x50414301
		puts "Invalid magic 0x%08X vs 0x50414301" % magic
		return 0
	end
	puts "	PAC\x01 data:"
	unk1 = readEU32(data, 4)
	size = readEU32(data, 8)
	unk3 = readEU32(data, 12)

	puts "		Unk1    : 0x%08X" % unk1
	puts "		Size    : 0x%08X" % size
	puts "		Unk3    : 0x%08X" % unk3
	
	outf = File.new(ARGV[1] + "_pac_%d.bin" % $curfile, "w+")
	outf.write(data[0...size])
	outf.close
	# Some sort of data pool used by the PAC\x01 data?
	size = align(size, 0x10)
	pool = data[size...size+0x800]
	outf = File.new(ARGV[1] + "_pac_%d_pool.bin" % $curfile, "w+")
	outf.write(pool)
	outf.close

	return size+0x800
end

def handle_riff(data, pfx)
	magic = readU32(data, 0)
	unless magic == 0x52494646
		puts "Invalid magic 0x%08X vs 0x52494646" % magic
		return 0
	end
	puts "	RIFF data:"
	size = readEU32(data, 4)
	type = readU32(data, 8)
	puts "		Size    : 0x%08X" % size
	puts "		Type    : 0x%08X" % type
	size += 0x8
	case type
		when 0x57415645 # WAVE
			outf = File.new(ARGV[1] + "_riff_%d.wav" % $curfile, "w+")
			outf.write(data[0...size])
			outf.close
		else
			outf = File.new(ARGV[1] + "_riff_%d.bin" % $curfile, "w+")
			outf.write(data[0...size])
			outf.close
	end
	# Some sort of data pool used by the PAC\x01 data?
	size = align(size, 0x10)
	pool = data[size...size+0x800]
	outf = File.new(ARGV[1] + "_riff_%d_pool.bin" % $curfile, "w+")
	outf.write(pool)
	outf.close

	return size+0x800
end

dataf = File.new(ARGV[0], "r")
data = dataf.read()
dataf.close

$curfile = 0
curoff = 0
dsize = data.length

while curoff < dsize
	puts "File %d:" % $curfile
	puts "	Offset: 0x%08X" % curoff
	magic = readU32(data, curoff)
	case magic
		when 0x50414301	# PAC\x01
			curoff += handle_pac(data[curoff..-1], ARGV[1])
		when 0x52494646	# RIFF
			curoff += handle_riff(data[curoff..-1], ARGV[1])
		else
			puts "Unknown magic 0x%08X" % magic
			puts "Quitting..."
			exit
	end
	$curfile += 1
end

